mod component;
mod config;
mod core;
mod deferred_handlers;
mod detect;
mod rectify;
mod world;

use crate::core::{core::Core, deploy::DeployCore};
use std::{
    path::{Path, PathBuf},
    process::exit,
    sync::{
        atomic::{AtomicBool, AtomicI64, AtomicU32, AtomicU64, AtomicU8, AtomicUsize, Ordering},
        Arc, Mutex, RwLock,
    },
    time::{Duration, SystemTime, UNIX_EPOCH},
};

use anyhow::bail;
use azalea::{
    app::PluginGroup,
    prelude::Component,
    protocol::packets::game::{ClientboundGamePacket, ServerboundGamePacket},
    Account, Client, ClientBuilder, DefaultBotPlugins, DefaultPlugins, Event, Item,
};
use azalea_physics::PhysicsPlugin;
use bevy_log::{info, LogPlugin};
use config::{DiskConfiguration, MemoryConfiguration};
use deferred_handlers::{
    handle_border_center, handle_border_initialize, handle_border_lerp_size, handle_border_size,
    handle_command_tree_packet, handle_entity_event, handle_game_event_packet, handle_login_packet,
    handle_set_carried_item_packet, handle_set_container_packet, handle_set_slot_packet,
    handle_system_chat_packet, on_client_tick, on_server_second,
};
use detect::icu::{on_movement_packet, IcuState};
use futures::future::join_all;
use notify::Watcher;
use rand::{distributions::Standard, thread_rng, Rng};
use tokio::{join, runtime::Runtime, time};
use tracing::{subscriber, warn, Level};

#[derive(Debug)]
pub struct Hotbar {
    pub slot: AtomicU8,
    pub contents: RwLock<[Item; 9]>,
}

impl Hotbar {
    pub fn read_current_hotbar_slot_contents(&self) -> anyhow::Result<Item> {
        Ok(self.contents.read().expect("Expected hotbar slot read")
            [self.slot.load(Ordering::Acquire) as usize])
    }
}

impl Default for Hotbar {
    fn default() -> Self {
        Self {
            slot: AtomicU8::new(0),
            contents: RwLock::new([Item::Air; 9]),
        }
    }
}

#[derive(Debug, Default)]
pub struct WorldBorder {
    pub size: AtomicU64,
    pub center_x: AtomicI64,
    pub center_z: AtomicI64,
}

#[derive(Debug, Default)]
pub struct WorldData {
    pub worldborder: WorldBorder,
}

#[derive(Debug, Default)]
pub struct SelfData {
    pub id: AtomicUsize,
    pub op: AtomicBool,
    pub gmc: AtomicBool,
    pub sequence: AtomicU32,
    pub hotbar: Hotbar,
    pub vanish: AtomicBool,
    pub icu: IcuState,
}

#[derive(Debug, Default, Component, Clone)]
pub struct State {
    pub tick_counter: Arc<AtomicUsize>,
    pub last_advertisement_tick: Arc<AtomicUsize>,
    pub runtime_configuration: Arc<MemoryConfiguration>,
    pub core: Arc<Core>,
    pub deploy_core: Arc<DeployCore>,
    pub self_data: Arc<SelfData>,
    pub world_data: Arc<WorldData>,
    pub packet_queue: Arc<Mutex<Vec<ServerboundGamePacket>>>,
}

impl State {
    fn from_runtime_configuration(runtime_configuration: Arc<MemoryConfiguration>) -> Self {
        Self {
            runtime_configuration,
            ..Default::default()
        }
    }
}

fn is_allowed_character(c: &u8) -> bool {
    !(*c <= 32 || *c >= 127)
}

fn generate_account() -> anyhow::Result<Account> {
    let mut rng = thread_rng();
    let len = rng.gen_range(3..17);
    let chars: Vec<u8> = rng
        .sample_iter::<u8, _>(Standard)
        .filter(|c| is_allowed_character(c))
        .take(len)
        .collect();

    Ok(Account::offline(&String::from_utf8(chars)?))
}

async fn handle(client: Client, event: Event, state: State) -> anyhow::Result<()> {
    match event {
        Event::Login => {
            state.deploy_core.recalculate_position(&client).await?;
            state.core.recalculate_positions(&client).await?;

            Ok(())
        }
        Event::Tick => {
            if !client.logged_in() {
                warn!("Ignoring pre-login tick");
                return Ok(());
            }

            let val = state.tick_counter.fetch_add(1, Ordering::Release) + 1;
            on_client_tick(&client, &state, val).await?;

            Ok(())
        }
        Event::Packet(packet) => match packet.as_ref() {
            ClientboundGamePacket::PlayerPosition(player_position) => {
                on_movement_packet(&client, player_position, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::Login(login) => {
                handle_login_packet(&client, login, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::EntityEvent(entity_event) => {
                handle_entity_event(&client, entity_event, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::SetTime(_) => {
                on_server_second(&client, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::GameEvent(game_event) => {
                handle_game_event_packet(&client, game_event, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::Commands(commands) => {
                handle_command_tree_packet(&client, commands, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::SetCarriedItem(carried_item) => {
                handle_set_carried_item_packet(&client, carried_item, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::ContainerSetSlot(set_slot) => {
                handle_set_slot_packet(&client, set_slot, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::ContainerSetContent(set_content) => {
                handle_set_container_packet(&client, set_content, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::SystemChat(system_chat) => {
                handle_system_chat_packet(&client, system_chat, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::InitializeBorder(initialize_border) => {
                handle_border_initialize(&client, initialize_border, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::SetBorderLerpSize(border_lerp_size) => {
                handle_border_lerp_size(&client, border_lerp_size, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::SetBorderSize(border_size) => {
                handle_border_size(&client, border_size, &state).await?;

                Ok(())
            }
            ClientboundGamePacket::SetBorderCenter(border_center) => {
                handle_border_center(&client, border_center, &state).await?;

                Ok(())
            }
            _ => Ok(()),
        },
        _ => Ok(()),
    }
}

fn timestamp() -> anyhow::Result<u128> {
    Ok(SystemTime::now().duration_since(UNIX_EPOCH)?.as_millis())
}

async fn launch(
    server: String,
    runtime_configuration: Arc<MemoryConfiguration>,
) -> anyhow::Result<()> {
    info!("Launching bot for {}", server);

    loop {
        info!("Generating account for {}", server);
        let account = generate_account()?;
        info!("Creating bot state for {}", server);
        let state = State::from_runtime_configuration(runtime_configuration.clone());
        info!("Bot state for {} created", server);
        let last_conn = timestamp()?;
        let client_err = ClientBuilder::new_without_plugins()
            .add_plugins(
                DefaultPlugins
                    .build()
                    .disable::<LogPlugin>()
                    .disable::<PhysicsPlugin>(),
            )
            .add_plugins(DefaultBotPlugins.build())
            .set_handler(handle)
            .set_state(state)
            .start(account, server.as_str())
            .await;

        if let Some(err) = client_err.err() {
            warn!("Error: {}", err);
        }

        let now = timestamp()?;
        let diff = now - last_conn;

        if diff > 6000 {
            continue;
        }

        let delay = 6000 - diff;
        warn!("Waiting for {}ms before reconnect", delay);
        time::sleep(Duration::from_millis(delay as u64)).await;
    }
}

async fn watch_config(
    configuration_path: PathBuf,
    memory_configuration: Arc<MemoryConfiguration>,
) -> anyhow::Result<()> {
    let configuration_path_clone = configuration_path.clone();
    let memory_configuration_clone = memory_configuration.clone();
    let mut watcher = notify::recommended_watcher(
        move |event: Result<notify::Event, notify::Error>| match event {
            Ok(event) => match event.kind {
                notify::EventKind::Modify(_) => {
                    let runtime = Runtime::new().expect("Expected runtime");
                    let disk_configuration = runtime
                        .block_on(DiskConfiguration::load(configuration_path.clone()))
                        .expect("Expected disk configuration");
                    runtime.block_on(memory_configuration_clone.apply(&disk_configuration));
                }
                notify::EventKind::Remove(_) => {}
                _ => {}
            },
            Err(_) => {}
        },
    )?;

    watcher
        .watch(
            configuration_path_clone.as_path(),
            notify::RecursiveMode::NonRecursive,
        )
        .expect("Expected watcher");

    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // Shamelessly stolen from the Azalea test bot. Thanks mat-1!
    {
        use parking_lot::deadlock;
        use std::thread;
        use std::time::Duration;
        // Create a background thread which checks for deadlocks every 10s
        thread::spawn(move || loop {
            thread::sleep(Duration::from_secs(10));
            let deadlocks = deadlock::check_deadlock();
            if deadlocks.is_empty() {
                continue;
            }

            println!("{} deadlocks detected", deadlocks.len());
            for (i, threads) in deadlocks.iter().enumerate() {
                println!("Deadlock #{i}");
                for t in threads {
                    println!("Thread Id {:#?}", t.thread_id());
                    println!("{:#?}", t.backtrace());
                }
            }
        });
    }

    subscriber::set_global_default(
        tracing_subscriber::FmtSubscriber::builder()
            .with_max_level(Level::INFO)
            .finish(),
    )?;

    let configuration_path = Path::new("config.json").to_path_buf();
    let configuration = DiskConfiguration::load(configuration_path.clone()).await?;
    let memory_configuration: MemoryConfiguration = (&configuration).into();
    let runtime_configuration_ref = Arc::new(memory_configuration);
    let mut futures = vec![];

    for server in configuration.servers.iter() {
        futures.push(launch(server.clone(), runtime_configuration_ref.clone()));
    }

    ctrlc::set_handler(|| {
        warn!("Killing program due to Ctrl+C signal");
        exit(-1);
    })?;

    let res = join!(
        join_all(futures),
        watch_config(configuration_path, runtime_configuration_ref),
    );

    if res.1.is_err() {
        bail!(
            "Error occurred in watch config future: {}",
            res.1.unwrap_err()
        );
    }

    Ok(())
}
