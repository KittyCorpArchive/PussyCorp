use azalea_chat::{
    base_component::BaseComponent,
    style::{ChatFormatting, Style, TextColor},
    text_component::TextComponent,
    translatable_component::{StringOrComponent, TranslatableComponent},
    FormattedText,
};

pub enum ComponentType {
    Translatable,
    Literal,
}

pub struct ComponentBuilder {
    kind: ComponentType,
    children: Vec<ComponentBuilder>,
    key: String,
    bold: Option<bool>,
    obfuscated: Option<bool>,
    italic: Option<bool>,
    underlined: Option<bool>,
    strikethrough: Option<bool>,
    color: Option<Color>,
    args: Vec<ComponentBuilder>,
}

impl ComponentBuilder {
    fn create(key: &str, kind: ComponentType) -> Self {
        Self {
            kind,
            children: vec![],
            key: key.to_owned(),
            bold: None,
            obfuscated: None,
            italic: None,
            underlined: None,
            strikethrough: None,
            color: None,
            args: vec![],
        }
    }

    #[must_use]
    pub fn literal(text: &str) -> Self {
        ComponentBuilder::create(text, ComponentType::Literal)
    }

    #[must_use]
    pub fn translatable(key: &str) -> Self {
        ComponentBuilder::create(key, ComponentType::Translatable)
    }

    #[must_use]
    pub fn empty() -> Self {
        ComponentBuilder::literal("")
    }

    #[must_use]
    pub fn build(&self) -> FormattedText {
        self.build_internal(0)
    }

    #[must_use]
    pub fn build_to_json(&self) -> anyhow::Result<String> {
        Ok(serde_json::to_string(&self.build())?)
    }

    #[must_use]
    pub fn append(mut self, builder: ComponentBuilder) -> Self {
        self.children.push(builder);

        self
    }

    #[must_use]
    pub fn bold(self) -> Self {
        self.set_bold(true)
    }

    #[must_use]
    pub fn set_bold(mut self, bold: bool) -> Self {
        self.bold = Some(bold);

        self
    }

    #[must_use]
    pub fn reset_bold(mut self) -> Self {
        self.bold = None;

        self
    }

    #[must_use]
    pub fn italic(self) -> Self {
        self.set_italic(true)
    }

    #[must_use]
    pub fn set_italic(mut self, italic: bool) -> Self {
        self.italic = Some(italic);

        self
    }

    #[must_use]
    pub fn reset_italic(mut self) -> Self {
        self.italic = None;

        self
    }

    #[must_use]
    pub fn underlined(self) -> Self {
        self.set_underlined(true)
    }

    #[must_use]
    pub fn set_underlined(mut self, underlined: bool) -> Self {
        self.underlined = Some(underlined);

        self
    }

    #[must_use]
    pub fn reset_underlined(mut self) -> Self {
        self.underlined = None;

        self
    }

    #[must_use]
    pub fn strikethrough(self) -> Self {
        self.set_strikethrough(true)
    }

    #[must_use]
    pub fn set_strikethrough(mut self, strikethrough: bool) -> Self {
        self.strikethrough = Some(strikethrough);

        self
    }

    #[must_use]
    pub fn reset_strikethrough(mut self) -> Self {
        self.strikethrough = None;

        self
    }

    #[must_use]
    pub fn set_color(mut self, color: Color) -> Self {
        self.color = Some(color);

        self
    }

    #[must_use]
    pub fn reset_color(mut self) -> Self {
        self.color = None;

        self
    }

    fn build_internal(&self, depth: usize) -> FormattedText {
        if depth > 128 {
            return ComponentBuilder::empty().build();
        }

        let children: Vec<FormattedText> = self
            .children
            .iter()
            .map(|b| b.build_internal(depth + 1))
            .collect();
        let args = self
            .args
            .iter()
            .map(|b| b.build_internal(depth + 1))
            .map(|f| StringOrComponent::FormattedText(f))
            .collect();
        let text_color;

        if let Some(color) = &self.color {
            text_color = Some(match color {
                Color::Hex(hex) => TextColor {
                    value: hex.color,
                    name: None,
                },
                Color::Named(named) => {
                    let named: ChatFormatting = named.clone().into();
                    TextColor {
                        name: Some(named.name().to_ascii_lowercase()),
                        value: named.color().unwrap_or(16777215),
                    }
                }
            });
        } else {
            text_color = None;
        }

        let base = BaseComponent {
            siblings: children,
            style: Style {
                color: text_color,
                bold: self.bold,
                italic: self.italic,
                underlined: self.underlined,
                strikethrough: self.strikethrough,
                obfuscated: self.obfuscated,
                reset: true,
            },
        };

        match self.kind {
            ComponentType::Translatable => FormattedText::Translatable(TranslatableComponent {
                base,
                key: self.key.clone(),
                args,
            }),
            ComponentType::Literal => FormattedText::Text(TextComponent {
                base,
                text: self.key.clone(),
            }),
        }
    }
}

impl Into<FormattedText> for ComponentBuilder {
    fn into(self) -> FormattedText {
        self.build()
    }
}

impl ToString for ComponentBuilder {
    fn to_string(&self) -> String {
        self.build_to_json().expect("Failed to serialize component")
    }
}

pub struct HexColor {
    color: u32,
}

#[derive(Clone)]
pub enum NamedColor {
    Black,
    DarkBlue,
    DarkGreen,
    DarkAqua,
    DarkRed,
    DarkPurple,
    Gold,
    Gray,
    DarkGray,
    Blue,
    Green,
    Aqua,
    Red,
    LightPurple,
    Yellow,
    White,
}

pub enum Color {
    Hex(HexColor),
    Named(NamedColor),
}

impl Into<ChatFormatting> for NamedColor {
    fn into(self) -> ChatFormatting {
        match self {
            NamedColor::Black => ChatFormatting::Black,
            NamedColor::DarkBlue => ChatFormatting::DarkBlue,
            NamedColor::DarkGreen => ChatFormatting::DarkGreen,
            NamedColor::DarkAqua => ChatFormatting::DarkAqua,
            NamedColor::DarkRed => ChatFormatting::DarkRed,
            NamedColor::DarkPurple => ChatFormatting::DarkPurple,
            NamedColor::Gold => ChatFormatting::Gold,
            NamedColor::Gray => ChatFormatting::Gray,
            NamedColor::DarkGray => ChatFormatting::DarkGray,
            NamedColor::Blue => ChatFormatting::Blue,
            NamedColor::Green => ChatFormatting::Green,
            NamedColor::Aqua => ChatFormatting::Aqua,
            NamedColor::Red => ChatFormatting::Red,
            NamedColor::LightPurple => ChatFormatting::LightPurple,
            NamedColor::Yellow => ChatFormatting::Yellow,
            NamedColor::White => ChatFormatting::White,
        }
    }
}

impl Color {
    pub fn hex(color: u32) -> Color {
        Color::Hex(HexColor { color })
    }

    pub fn named(color: NamedColor) -> Color {
        Color::Named(color)
    }
}
