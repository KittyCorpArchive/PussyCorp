use std::sync::atomic::Ordering;

use azalea::Client;
use tracing::info;

use crate::State;

pub async fn rectify(client: &Client, state: &State, count: usize) -> anyhow::Result<()> {
    // Wait 1s before attempting any rectification operations
    if count < 20 {
        return Ok(());
    }

    // Do not attempt to rectify any invalid states outside of the defined correction interval
    if count
        % state
            .runtime_configuration
            .correction_interval
            .load(Ordering::Acquire)
        != 0
    {
        return Ok(());
    }

    if !state.self_data.op.load(Ordering::Acquire) {
        info!("Trying to OP self...");
        client.chat("/minecraft:op @s[type=player]");
        return Ok(());
    }

    if !state.self_data.gmc.load(Ordering::Acquire) {
        info!("Trying to enter gamemode creative...");
        client.chat("/minecraft:gamemode creative");
        return Ok(());
    }

    state.deploy_core.refill_if_needed(client, state).await?;
    state.core.refill_if_needed(client, state).await?;

    if !state.self_data.vanish.load(Ordering::Acquire) {
        info!("Trying to enter vanish...");
        state
            .core
            .execute(state, &format!("/vanish {} enable", client.profile.name))
            .await?;
    }

    Ok(())
}
