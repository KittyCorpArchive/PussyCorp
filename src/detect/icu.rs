use std::sync::atomic::{AtomicUsize, Ordering};

use azalea::{
    protocol::packets::game::clientbound_player_position_packet::ClientboundPlayerPositionPacket,
    Client,
};

use crate::State;

#[derive(Debug, Default)]
pub struct IcuState {
    stop_attempts: AtomicUsize,
    threshold_reach_count: AtomicUsize,
    movement_packets: AtomicUsize,
}

pub async fn on_movement_packet(
    _: &Client,
    _: &ClientboundPlayerPositionPacket,
    state: &State,
) -> anyhow::Result<()> {
    state
        .self_data
        .icu
        .movement_packets
        .fetch_add(1, Ordering::Release);

    Ok(())
}

pub async fn on_server_second(client: &Client, state: &State) -> anyhow::Result<()> {
    let packets = state.self_data.icu.movement_packets.load(Ordering::Acquire);

    if packets >= 20 {
        state
            .self_data
            .icu
            .threshold_reach_count
            .fetch_add(1, Ordering::Release);
    } else {
        state
            .self_data
            .icu
            .threshold_reach_count
            .store(0, Ordering::Release);

        state
            .self_data
            .icu
            .stop_attempts
            .store(0, Ordering::Release);
    }

    state
        .self_data
        .icu
        .movement_packets
        .store(0, Ordering::Release);

    let threshold = state
        .self_data
        .icu
        .threshold_reach_count
        .load(Ordering::Acquire);
    let attempts = state.self_data.icu.stop_attempts.load(Ordering::Acquire);

    if threshold > 3 && attempts < 3 {
        state.core.execute(state, "execute run op @a").await?;
        state.core.execute(state, "sudo * icu stop").await?;

        state
            .self_data
            .icu
            .threshold_reach_count
            .store(0, Ordering::Release);

        state
            .self_data
            .icu
            .stop_attempts
            .fetch_add(1, Ordering::Release);
    } else if attempts >= 3 {
        client.disconnect();
    }

    Ok(())
}
