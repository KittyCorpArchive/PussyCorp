use std::{
    ops::Sub,
    sync::{atomic::Ordering, RwLock},
};

use azalea::{
    blocks::BlockState,
    inventory::ItemSlotData,
    protocol::packets::game::{
        serverbound_interact_packet::InteractionHand,
        serverbound_player_action_packet::{Action, ServerboundPlayerActionPacket},
        serverbound_set_creative_mode_slot_packet::ServerboundSetCreativeModeSlotPacket,
        serverbound_use_item_on_packet::{BlockHit, ServerboundUseItemOnPacket},
    },
    BlockPos, Client, Item,
};
use azalea_core::Direction;
use azalea_nbt::Nbt;
use tracing::warn;

use crate::{
    world::util::{execute_command, is_air_block, is_core_block, is_core_item, is_strange_block},
    State,
};

use super::core::MutBlockPos;

#[derive(Debug, Default)]
pub struct DeployCore {
    pub block_pos: RwLock<MutBlockPos>,
}

impl DeployCore {
    pub async fn recalculate_position(&self, client: &Client) -> anyhow::Result<()> {
        let self_pos: BlockPos = client.position().into();
        let sub = self_pos.sub(BlockPos::new(0, 3, 0));

        self.block_pos
            .write()
            .expect("Expected block pos write")
            .apply(sub.into());

        Ok(())
    }

    pub fn execute(&self, state: &State, command: &str) -> anyhow::Result<()> {
        execute_command(
            state,
            command,
            (*self.block_pos.read().expect("Expected block pos read")).into(),
        )
    }

    fn place(
        &self,
        client: &Client,
        block_state_opt: Option<BlockState>,
        core_pos: BlockPos,
        strange: bool,
        state: &State,
    ) -> anyhow::Result<()> {
        if strange {
            warn!(
                "Replacing strange block at {} {} {} with air!",
                core_pos.x, core_pos.y, core_pos.z
            );
            // handle it on next rebuild
            client.chat(&format!(
                "/setblock {} {} {} air",
                core_pos.x, core_pos.y, core_pos.z
            ));

            return Ok(());
        }

        let mut packet_queue = state
            .packet_queue
            .lock()
            .expect("Expected packet queue lock");
        let selected_hotbar_item = state.self_data.hotbar.read_current_hotbar_slot_contents()?;
        let is_air;

        if let Some(block_state) = block_state_opt {
            is_air = is_air_block(&block_state)?;
        } else {
            is_air = true; // we can assume that non-present values are air blocks
        }

        if !is_air {
            let packet = ServerboundPlayerActionPacket {
                sequence: state.self_data.sequence.fetch_add(1, Ordering::Release),
                action: Action::StartDestroyBlock,
                pos: core_pos,
                direction: Direction::North,
            }
            .get();

            packet_queue.push(packet);

            let packet = ServerboundPlayerActionPacket {
                sequence: state.self_data.sequence.fetch_add(1, Ordering::Release),
                action: Action::StopDestroyBlock,
                pos: core_pos,
                direction: Direction::North,
            }
            .get();

            packet_queue.push(packet);
        }

        let is_core_selected = is_core_item(&selected_hotbar_item)?;

        if !is_core_selected {
            let packet = ServerboundSetCreativeModeSlotPacket {
                slot_num: (36 + state.self_data.hotbar.slot.load(Ordering::Acquire)) as u16,
                item_stack: azalea::inventory::ItemSlot::Present(ItemSlotData {
                    kind: Item::RepeatingCommandBlock,
                    count: 1,
                    nbt: Nbt::End,
                }),
            }
            .get();

            packet_queue.push(packet);
        }

        let packet = ServerboundUseItemOnPacket {
            sequence: state.self_data.sequence.fetch_add(1, Ordering::Release),
            hand: InteractionHand::MainHand,
            block_hit: BlockHit {
                block_pos: core_pos,
                direction: Direction::North,
                location: core_pos.center(),
                inside: false,
            },
        }
        .get();

        packet_queue.push(packet);

        Ok(())
    }

    pub async fn refill_if_needed(&self, client: &Client, state: &State) -> anyhow::Result<()> {
        let block_pos = self
            .block_pos
            .read()
            .expect("Expected block pos read")
            .clone();
        let actual_block_pos: BlockPos = block_pos.into();
        let world = client.world();
        let world_lock = world.read();
        let chunks = &world_lock.chunks;
        let block_state_opt = chunks.get_block_state(&actual_block_pos);
        let mut strange = false;

        if let Some(block_state) = block_state_opt {
            if is_core_block(&block_state)? {
                return Ok(());
            }

            strange = is_strange_block(&block_state)?;
        }

        self.place(client, block_state_opt, actual_block_pos, strange, state)
    }
}
