use azalea::{
    blocks::{Block, BlockState},
    protocol::packets::game::serverbound_set_command_block_packet::{
        Mode, ServerboundSetCommandBlockPacket,
    },
    Item,
};
use azalea_core::BlockPos;

use crate::State;

pub fn get_block_from_state(block_state: &BlockState) -> anyhow::Result<Box<dyn Block>> {
    Ok((*block_state).into())
}

pub fn is_core_block(block_state: &BlockState) -> anyhow::Result<bool> {
    let block = get_block_from_state(block_state)?;

    Ok(block.id() == "repeating_command_block" || block.id() == "command_block")
}

// TODO: Name this better
pub fn is_strange_block(block_state: &BlockState) -> anyhow::Result<bool> {
    let block = get_block_from_state(block_state)?;

    // TODO: Investigate if we need more block ids here, or if this will suffice
    Ok(block.id() == "moving_piston")
}

pub fn is_core_item(item: &Item) -> anyhow::Result<bool> {
    let item_deref = *item;

    Ok(item_deref == azalea::Item::RepeatingCommandBlock
        || item_deref == azalea::Item::ChainCommandBlock
        || item_deref == azalea::Item::CommandBlock)
}

pub fn is_air_block(block_state: &BlockState) -> anyhow::Result<bool> {
    let block = get_block_from_state(block_state)?;

    Ok(block.id() == "cave_air" || block.id() == "air")
}

pub fn execute_command(state: &State, command: &str, block_pos: BlockPos) -> anyhow::Result<()> {
    let packet = ServerboundSetCommandBlockPacket {
        pos: block_pos,
        command: command.to_owned(),
        mode: Mode::Auto,
        track_output: false,
        conditional: false,
        automatic: true,
    }
    .get();

    state
        .packet_queue
        .lock()
        .expect("Expected packet queue lock")
        .push(packet);

    Ok(())
}
