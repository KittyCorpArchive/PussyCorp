use std::{
    path::PathBuf,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
};

use serde::{Deserialize, Serialize};
use tokio::{
    fs::{File, OpenOptions},
    io::{AsyncReadExt, AsyncWriteExt},
    sync::RwLock,
};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct CorrectionSettings {
    pub interval: usize,
}

impl Default for CorrectionSettings {
    fn default() -> Self {
        Self {
            interval: 4, /* 200 ms */
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct AdvertisementSettings {
    pub message: String,
    pub interval: usize,
}

impl Default for AdvertisementSettings {
    fn default() -> Self {
        Self {
            message: "Join the KittyCorp Discord at dsc.gg/kittycorp".to_owned(),
            interval: ((1000 / 50) * 60) * 5, // Every 5 minutes
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DiskConfiguration {
    pub servers: Vec<String>,
    pub config_version: usize,
    pub advertisement: AdvertisementSettings,
    pub correction: CorrectionSettings,
}

#[derive(Debug, Default, Clone)]
pub struct MemoryConfiguration {
    pub advertisement_message: Arc<RwLock<String>>,
    pub advertisement_interval: Arc<AtomicUsize>,
    pub correction_interval: Arc<AtomicUsize>,
}

impl MemoryConfiguration {
    pub async fn apply(&self, disk_configuration: &DiskConfiguration) {
        let current_advertisement_message = self.advertisement_message.read().await.clone();

        if !(current_advertisement_message == disk_configuration.advertisement.message) {
            let mut write = self.advertisement_message.write().await;

            write.clear();
            write.push_str(&disk_configuration.advertisement.message);
            drop(write)
        }

        let current_advertisement_interval = self.advertisement_interval.load(Ordering::Acquire);

        if !(current_advertisement_interval == disk_configuration.advertisement.interval) {
            self.advertisement_interval
                .store(disk_configuration.advertisement.interval, Ordering::Release);
        }

        let current_correction_interval = self.correction_interval.load(Ordering::Acquire);

        if !(current_correction_interval == disk_configuration.correction.interval) {
            self.advertisement_interval
                .store(disk_configuration.advertisement.interval, Ordering::Release);
        }
    }
}

impl Into<MemoryConfiguration> for &DiskConfiguration {
    fn into(self) -> MemoryConfiguration {
        MemoryConfiguration {
            advertisement_interval: Arc::new(AtomicUsize::new(self.advertisement.interval)),
            advertisement_message: Arc::new(RwLock::new(self.advertisement.message.clone())),
            correction_interval: Arc::new(AtomicUsize::new(self.correction.interval)),
        }
    }
}

impl Default for DiskConfiguration {
    fn default() -> Self {
        Self {
            config_version: 1,
            servers: vec!["localhost".to_owned()],
            advertisement: AdvertisementSettings::default(),
            correction: CorrectionSettings::default(),
        }
    }
}

impl DiskConfiguration {
    pub async fn load(path: PathBuf) -> anyhow::Result<DiskConfiguration> {
        if !path.exists() {
            let default_config = DiskConfiguration::default();
            default_config.save(path).await?;
            return Ok(default_config);
        }

        let mut file = File::open(path).await?;
        let len = file.metadata().await?.len() as usize;
        let mut data = vec![0; len];
        file.read_exact(&mut data).await?;
        let as_string = String::from_utf8(data)?;
        let deserialized: DiskConfiguration = serde_json::from_str(&as_string)?;

        Ok(deserialized)
    }

    pub async fn save(&self, path: PathBuf) -> anyhow::Result<()> {
        let serialized = serde_json::to_string_pretty(self)?;
        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(path)
            .await?;
        let as_utf8 = serialized.as_bytes();
        file.write_all(as_utf8).await?;

        Ok(())
    }
}
