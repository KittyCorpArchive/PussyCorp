use std::{
    ops::{Add, Sub},
    sync::RwLock,
};

use azalea::{BlockPos, Client};
use rand::{thread_rng, Rng};

use crate::{
    world::util::{execute_command, is_core_block},
    State,
};

#[derive(Debug, Default)]
pub struct Core {
    first_pos: RwLock<MutBlockPos>,
    second_pos: RwLock<MutBlockPos>,
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash)]
pub struct MutBlockPos {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl MutBlockPos {
    pub fn apply(&mut self, other_pos: MutBlockPos) {
        self.x = other_pos.x;
        self.y = other_pos.y;
        self.z = other_pos.z;
    }
}

impl From<BlockPos> for MutBlockPos {
    fn from(value: BlockPos) -> Self {
        Self {
            x: value.x,
            y: value.y,
            z: value.z,
        }
    }
}

impl Into<BlockPos> for MutBlockPos {
    fn into(self) -> BlockPos {
        BlockPos {
            x: self.x,
            y: self.y,
            z: self.z,
        }
    }
}

// TODO: Implement detection for when the core is outside of the bot's render distance and as such is unable to be monitored
// TODO: Fix outer layer of core not being refilled
// TODO: Ratelimit refills

impl Core {
    pub fn set_first_pos(&self, block_pos: impl Into<BlockPos>) -> anyhow::Result<()> {
        let block_pos = block_pos.into();
        let mut first_pos = self.first_pos.write().expect("Expected first pos");

        first_pos.x = block_pos.x;
        first_pos.y = block_pos.y;
        first_pos.z = block_pos.z;
        Ok(())
    }

    pub fn set_second_pos(&self, block_pos: impl Into<BlockPos>) -> anyhow::Result<()> {
        let block_pos = block_pos.into();
        let mut second_pos = self.second_pos.write().expect("Expected second pos");

        second_pos.x = block_pos.x;
        second_pos.y = block_pos.y;
        second_pos.z = block_pos.z;
        Ok(())
    }

    pub fn get_first_pos(&self) -> anyhow::Result<BlockPos> {
        let first_pos = self.first_pos.read().expect("Expected first pos");

        Ok(first_pos.clone().into())
    }

    pub fn get_second_pos(&self) -> anyhow::Result<BlockPos> {
        let second_pos = self.second_pos.read().expect("Expected first pos");

        Ok(second_pos.clone().into())
    }

    async fn get_world_height(client: &Client) -> anyhow::Result<i32> {
        let world = client.world();
        let world_lock = world.read();
        let min_y = world_lock.chunks.min_y;

        Ok(min_y)
    }

    pub async fn execute(&self, state: &State, command: &str) -> anyhow::Result<()> {
        let first_pos = self.get_first_pos()?;
        let second_pos = self.get_second_pos()?;

        let x_range = first_pos.x..=second_pos.x;
        let y_range = first_pos.y..=second_pos.y;
        let z_range = first_pos.z..=second_pos.z;
        let mut rng = thread_rng();
        let x = rng.gen_range(x_range);
        let y = rng.gen_range(y_range);
        let z = rng.gen_range(z_range);
        let block_pos = BlockPos::new(x, y, z);
        execute_command(state, command, block_pos)
    }

    pub async fn recalculate_positions(&self, client: &Client) -> anyhow::Result<()> {
        let min_y = Core::get_world_height(client).await?;
        let pos = client.position();
        let as_block_pos: BlockPos = pos.into();
        let with_min_y = BlockPos::new(as_block_pos.x, min_y, as_block_pos.z);
        let fifteen_big = BlockPos::new(15, 0, 15);
        let first_pos = with_min_y.sub(fifteen_big);
        let second_pos = with_min_y.add(fifteen_big);
        self.set_first_pos(first_pos)?;
        self.set_second_pos(second_pos)?;

        Ok(())
    }

    pub async fn refill_if_needed(&self, client: &Client, state: &State) -> anyhow::Result<()> {
        let refill = self.needs_refilling(client).await?;

        if !refill {
            return Ok(());
        }

        self.refill(state).await?;
        Ok(())
    }

    pub async fn needs_refilling(&self, client: &Client) -> anyhow::Result<bool> {
        let world = client.world();
        let read = world.read();
        let first_pos = self.first_pos.read().expect("Expected first pos");
        let second_pos = self.second_pos.read().expect("Expected second pos");

        for x in first_pos.x..=second_pos.x {
            for y in first_pos.y..=second_pos.y {
                for z in first_pos.z..=second_pos.z {
                    let pos = BlockPos::new(x, y, z);
                    if let Some(block_state) = read.chunks.get_block_state(&pos) {
                        if is_core_block(&block_state)? {
                            continue;
                        } else {
                            return Ok(true);
                        }
                    } else {
                        return Ok(true);
                    }
                }
            }
        }

        Ok(false)
    }

    pub async fn refill(&self, state: &State) -> anyhow::Result<()> {
        let block_pos: BlockPos = state
            .deploy_core
            .block_pos
            .read()
            .expect("Block pos read expected")
            .clone()
            .into();
        let first_pos = self.first_pos.read().expect("Expected first pos");
        let second_pos = self.second_pos.read().expect("Expected second pos");

        let command = format!(
            "/fill ~{} ~{} ~{} ~{} ~{} ~{} repeating_command_block",
            first_pos.x - block_pos.x,
            first_pos.y - block_pos.y,
            first_pos.z - block_pos.z,
            second_pos.x - block_pos.x,
            second_pos.y - block_pos.y,
            second_pos.z - block_pos.z
        );

        state.deploy_core.execute(state, &command)?;

        Ok(())
    }

    pub async fn create(client: &Client) -> anyhow::Result<Self> {
        let inst = Self {
            ..Default::default()
        };

        inst.recalculate_positions(client).await?;
        Ok(inst)
    }
}
