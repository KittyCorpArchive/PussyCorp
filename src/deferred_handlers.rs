use std::{ops::RangeInclusive, sync::atomic::Ordering};

use azalea::{
    protocol::packets::game::{
        clientbound_commands_packet::ClientboundCommandsPacket,
        clientbound_container_set_content_packet::ClientboundContainerSetContentPacket,
        clientbound_container_set_slot_packet::ClientboundContainerSetSlotPacket,
        clientbound_entity_event_packet::ClientboundEntityEventPacket,
        clientbound_game_event_packet::{ClientboundGameEventPacket, EventType},
        clientbound_initialize_border_packet::ClientboundInitializeBorderPacket,
        clientbound_login_packet::ClientboundLoginPacket,
        clientbound_set_border_center_packet::ClientboundSetBorderCenterPacket,
        clientbound_set_border_lerp_size_packet::ClientboundSetBorderLerpSizePacket,
        clientbound_set_border_size_packet::ClientboundSetBorderSizePacket,
        clientbound_set_carried_item_packet::ClientboundSetCarriedItemPacket,
        clientbound_system_chat_packet::ClientboundSystemChatPacket,
    },
    Client,
};
use tracing::{info, warn};

use crate::{
    component::{Color, ComponentBuilder},
    detect::icu,
    rectify::rectify,
    State, WorldBorder,
};

pub async fn on_client_tick(client: &Client, state: &State, count: usize) -> anyhow::Result<()> {
    let packet_opt = state
        .packet_queue
        .lock()
        .expect("Expected packet queue lock")
        .pop();

    if let Some(packet) = packet_opt {
        client.write_packet(packet);
    }

    rectify(client, state, count).await?;

    Ok(())
}

pub async fn on_server_second(client: &Client, state: &State) -> anyhow::Result<()> {
    state.deploy_core.recalculate_position(client).await?;
    state.core.recalculate_positions(client).await?;
    icu::on_server_second(client, state).await?;

    let current_tick: usize = state.tick_counter.load(Ordering::Acquire);
    let last_advertisement_tick = state.last_advertisement_tick.load(Ordering::Acquire);
    let advertisement_interval = state
        .runtime_configuration
        .advertisement_interval
        .load(Ordering::Acquire);
    let diff = current_tick - last_advertisement_tick;

    if diff < advertisement_interval {
        return Ok(());
    }

    state
        .last_advertisement_tick
        .store(current_tick, Ordering::Release);
    let component = ComponentBuilder::literal(
        &state
            .runtime_configuration
            .advertisement_message
            .read()
            .await,
    )
    .set_color(Color::hex(0x692600))
    .to_string();
    let as_json = "/tellraw @a ".to_owned() + &component;
    state.core.execute(state, &as_json).await?;

    Ok(())
}

pub async fn handle_login_packet(
    _: &Client,
    packet: &ClientboundLoginPacket,
    state: &State,
) -> anyhow::Result<()> {
    state
        .self_data
        .id
        .store(packet.player_id as usize, Ordering::Release);
    state
        .self_data
        .gmc
        .store(packet.game_type.to_id() == 1, Ordering::Release);
    info!("Set player ID to {}", packet.player_id);
    Ok(())
}

pub async fn handle_entity_event(
    _: &Client,
    packet: &ClientboundEntityEventPacket,
    state: &State,
) -> anyhow::Result<()> {
    let self_id = state.self_data.id.load(Ordering::Acquire);

    if packet.entity_id as usize != self_id {
        return Ok(());
    }

    match packet.event_id {
        24 | 25 | 26 | 27 => {
            info!("Noticed DeOP");
            state.self_data.op.store(false, Ordering::Release);
        }
        28 => {
            info!("Noticed OP");
            state.self_data.op.store(true, Ordering::Release);
        }
        _ => {}
    }

    Ok(())
}

pub async fn handle_command_tree_packet(
    _: &Client,
    packet: &ClientboundCommandsPacket,
    state: &State,
) -> anyhow::Result<()> {
    let is_op = packet
        .entries
        .iter()
        .find(|node| node.name().is_some_and(|name| name == "deop"))
        .is_some();

    if is_op {
        info!("The deop node is contained in the command tree");
    } else {
        info!("The deop node is not contained in the command tree");
    }

    state.self_data.op.store(is_op, Ordering::Release);
    Ok(())
}

pub async fn handle_game_event_packet(
    _: &Client,
    packet: &ClientboundGameEventPacket,
    state: &State,
) -> anyhow::Result<()> {
    match packet.event {
        EventType::ChangeGameMode => {
            let val = packet.param;

            if val == 1f32 {
                info!("Noticed GMC");
                state.self_data.gmc.store(true, Ordering::Release);
            } else {
                info!("Noticed Non-GMC");
                state.self_data.gmc.store(false, Ordering::Release);
            }
        }
        _ => return Ok(()),
    }

    Ok(())
}

pub async fn handle_set_carried_item_packet(
    _: &Client,
    packet: &ClientboundSetCarriedItemPacket,
    state: &State,
) -> anyhow::Result<()> {
    state
        .self_data
        .hotbar
        .slot
        .store(packet.slot, Ordering::Release);

    Ok(())
}

// implementing my own hotbar tracker because in all honesty i would rather not deal with bevy components today
// TODO: Figure out a way to use the terrible built-in inventory system without causing non-deterministic behaviour

fn get_real_slot(slot_range: RangeInclusive<usize>, slot: &usize) -> anyhow::Result<Option<usize>> {
    let slot_vec: Vec<usize> = slot_range.collect();

    Ok(slot_vec.iter().position(|e| e == slot))
}

fn get_hotbar_slots_range(client: &Client) -> anyhow::Result<RangeInclusive<usize>> {
    let menu = client.menu();

    Ok(menu.hotbar_slots_range())
}

pub async fn handle_set_slot_packet(
    client: &Client,
    packet: &ClientboundContainerSetSlotPacket,
    state: &State,
) -> anyhow::Result<()> {
    let slots_range = get_hotbar_slots_range(&client)?;
    let usize_slot = packet.slot as usize;

    if slots_range.contains(&usize_slot) {
        return Ok(());
    }

    let real_slot_opt = get_real_slot(slots_range, &usize_slot)?;

    if let Some(real_slot) = real_slot_opt {
        state
            .self_data
            .hotbar
            .contents
            .write()
            .expect("Expected hotbar contents write")[real_slot] = packet.item_stack.kind();
    } else {
        // TODO: Fix tracking of offhand slot
        warn!(
            "Could not find real slot value for {} in set slot packet",
            usize_slot
        );
    }

    Ok(())
}

pub async fn handle_set_container_packet(
    client: &Client,
    packet: &ClientboundContainerSetContentPacket,
    state: &State,
) -> anyhow::Result<()> {
    let slots_range = get_hotbar_slots_range(&client)?;
    let mut index = 0;

    for slot in packet.items.iter() {
        info!("Handling set container packet with index of {}", index);

        if !slots_range.contains(&index) {
            info!("Skipping update for {} in set container packet", index);
            index += 1;
            continue;
        }

        let real_slot_opt = get_real_slot(slots_range.clone(), &index)?;

        if let Some(real_slot) = real_slot_opt {
            state
                .self_data
                .hotbar
                .contents
                .write()
                .expect("Expected hotbar contents write")[real_slot] = slot.kind();
        } else {
            warn!(
                "Could not find real slot value for {} in container content set",
                index
            );
        }

        index += 1;
    }

    state
        .self_data
        .hotbar
        .contents
        .write()
        .expect("Expected htobar contents write")
        [state.self_data.hotbar.slot.load(Ordering::Acquire) as usize] = packet.carried_item.kind();

    Ok(())
}

pub async fn handle_system_chat_packet(
    client: &Client,
    packet: &ClientboundSystemChatPacket,
    state: &State,
) -> anyhow::Result<()> {
    let content = packet.content.to_string();

    if !content.starts_with("Vanish for ") {
        return Ok(());
    }

    // TODO: Implement display name assertion for vanish disable

    /*let tab_list = client.component::<TabList>();
    let mut display_name = tab_list
        .iter()
        .filter(|p| p.0.clone() == client.profile.uuid)
        .map(|p| (p.0.clone(), p.1.clone())) // convert from reference into owned type
        .map(|p| {
            p.1.display_name
                .unwrap_or_else(|| {
                    FormattedText::Text(legacy_color_code_to_text_component(&p.1.profile.name))
                })
                .to_string()
        })
        .last()
        .unwrap_or_else(|| client.profile.name.clone());*/

    if content.ends_with(": disabled")
    /*&& content.contains(&display_name)*/
    {
        info!("Noticed unvanish");
        state.self_data.vanish.store(false, Ordering::Release);
        return Ok(());
    }

    // the vanish description message is sent seperately so we can do this - https://github.com/EssentialsX/Essentials/blob/8b2c7d7ad14271c9e6ad4a901db7e6fb2182139e/Essentials/src/main/java/com/earth2me/essentials/commands/Commandvanish.java#L41
    if content.ends_with(": enabled") && content.contains(&client.profile.name) {
        info!("Noticed vanish");
        state.self_data.vanish.store(true, Ordering::Release);
        return Ok(());
    }

    Ok(())
}

async fn handle_worldborder_update(
    worldborder: &WorldBorder,
    client: &Client,
    _: &State,
) -> anyhow::Result<()> {
    // TODO: Make smarter

    if worldborder.size.load(Ordering::Acquire) < 59999968 {
        client.chat("/worldborder set 59999968");
    }

    Ok(())
}

async fn set_border_size(new_size: f64, client: &Client, state: &State) -> anyhow::Result<()> {
    state
        .world_data
        .worldborder
        .size
        .store(new_size.ceil() as u64, Ordering::Release);

    info!("Set worldborder size to {}", new_size);
    handle_worldborder_update(&state.world_data.worldborder, client, state).await?;
    Ok(())
}

async fn set_border_center(
    new_x: f64,
    new_z: f64,
    client: &Client,
    state: &State,
) -> anyhow::Result<()> {
    state
        .world_data
        .worldborder
        .center_x
        .store(new_x.ceil() as i64, Ordering::Release);
    state
        .world_data
        .worldborder
        .center_z
        .store(new_z.ceil() as i64, Ordering::Release);

    info!("Set worldborder center to (x: {}, z: {})", new_x, new_z);
    handle_worldborder_update(&state.world_data.worldborder, client, state).await?;
    Ok(())
}

pub async fn handle_border_initialize(
    client: &Client,
    packet: &ClientboundInitializeBorderPacket,
    state: &State,
) -> anyhow::Result<()> {
    set_border_size(packet.new_size, client, state).await?;
    set_border_center(packet.new_center_x, packet.new_center_z, client, state).await?;

    Ok(())
}

pub async fn handle_border_lerp_size(
    client: &Client,
    packet: &ClientboundSetBorderLerpSizePacket,
    state: &State,
) -> anyhow::Result<()> {
    set_border_size(packet.new_size, client, state).await?;

    Ok(())
}

pub async fn handle_border_size(
    client: &Client,
    packet: &ClientboundSetBorderSizePacket,
    state: &State,
) -> anyhow::Result<()> {
    set_border_size(packet.size, client, state).await?;

    Ok(())
}

pub async fn handle_border_center(
    client: &Client,
    packet: &ClientboundSetBorderCenterPacket,
    state: &State,
) -> anyhow::Result<()> {
    set_border_center(packet.new_center_x, packet.new_center_z, client, state).await?;

    Ok(())
}
